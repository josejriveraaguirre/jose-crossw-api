package com.jjr.josecrosswapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JosecrosswapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(JosecrosswapiApplication.class, args);
	}

}
