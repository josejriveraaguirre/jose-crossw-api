package com.jjr.josecrosswapi.repos;

import com.jjr.josecrosswapi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Users extends JpaRepository<User, Long> {

    User findByUsername(String username);
}

